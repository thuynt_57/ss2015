package src.basic;

/**
 * Created by dse on 04/12/2015.
 */
public class Worker implements Runnable {

    @Override
    public void run() {
        long sum = 0;
        for (int i = 0; i < 10000000; i++) {
            sum = sum + i; // do some work
            // every n iterations, print an update
            if (i % 1000000 == 0) {
                System.out.println(Thread.currentThread().getName() + " " + i);
            }
        }
    }
}