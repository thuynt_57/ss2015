package src.basic;

/**
 * Created by dse on 04/12/2015.
 */
public class DemoMultithreading {
    public static void main(String[] args) {
        Thread a = new Thread(new Worker());
        Thread b = new Thread(new Worker());

        System.out.println(Thread.currentThread().getName() + " starting workers...");
        a.start();
        b.start();
        // The current running thread (executing main()) blocks
        // until both workers have finished
//        try {
//            a.join();
//            b.join();
//        }
//        catch (Exception ignored) {}
        System.out.println(Thread.currentThread().getName() + " All done");
    }
}
