package src.gui;

import java.awt.*;
import java.io.IOException;
import java.nio.CharBuffer;


public class DrawingApp {


	public static void main(String[] args) {
		Shape shape1 = new Rectangle(.5, .5, .2, .3);
		shape1.setColor(Color.BLACK);
		shape1.draw();
		Shape shape2 = new Circle(.2, .2, .3);
		shape2.setColor(Color.green);
		shape2.draw();
		Shape shape = new Rectangle(.5, .5, .2, .3);
    	
        Thread a = new Thread() {
    		// TODO Auto-generated method stub 
        	public void run(){

        		synchronized(shape) {
		            try {
		                Thread.currentThread().sleep(1000);               
		            } catch (InterruptedException e) {  }
	            	
		            shape1.setColor(Color.green);  
		            shape1.draw();
		            
		            shape.notify();
		            try {
						shape.wait();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
        		}
	            
        	}
        };
        
        
        Thread b = new Thread() {
    		// TODO Auto-generated method stub 
        	public void run(){
        		synchronized(shape) {
	            	try {
						shape.wait();
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

		            
		            shape2.setColor(Color.BLACK); 
		            shape2.draw();
		            shape.notify();
        		}
	            
        	}
        };
        b.start();
        a.start();
    }
}

class Worker implements Runnable {
	private Shape shape;
	public void setShape(Shape _shape) {
		shape = _shape;
	}

	@Override
	public synchronized void run() {
		// TODO Auto-generated method stub        
        while (true) {
            try {
                Thread.currentThread().sleep(1000);               
            } catch (InterruptedException e) {  }
            
            shape.changeColor();
            shape.draw();    
        }
	}
}