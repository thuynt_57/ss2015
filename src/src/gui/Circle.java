package src.gui;

import java.awt.*;

public class Circle extends Shape {
    private double radius = .2;

    public Circle(double x, double y, double r) {
        super(x, y);
        this.radius = r;
    }

    public void draw() {
        setupDrawing();
        StdDraw.circle(x, y, radius);
    }

    @Override
    public void changeColor() {
        color = new Color((color.getRed()+1) % 256, color.getGreen(), color.getBlue());
    }
}
