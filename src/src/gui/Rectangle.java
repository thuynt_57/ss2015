package src.gui;

import java.awt.*;


/**
 * Created by dse on 26/11/2015.
 */
public class Rectangle extends Shape {
    private double halfWidth = .3;
    private double halfHeight = .2;

    public Rectangle(double x, double y, double hW, double hH) {
        super(x, y);
        this.halfHeight = hH;
        this.halfWidth = hW;
    }

    public void draw() {
        setupDrawing();
        StdDraw.rectangle(x, y, halfWidth, halfHeight);
    }

    @Override
     public void changeColor() {
        color = new Color(color.getRed(), color.getGreen(), (color.getBlue()+1) % 256);
    }
}
