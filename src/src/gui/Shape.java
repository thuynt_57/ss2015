package src.gui;

import java.awt.*;

/**
 * Created by dse on 26/11/2015.
 */
public abstract class Shape {
    protected Color color = StdDraw.BLUE;
    protected double penRadius = .02;
    protected double x = .6;
    protected double y = .5;

    public Shape(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    protected void setupDrawing() {
        if (color != null) StdDraw.setPenColor(color);
        else StdDraw.setPenColor();
        StdDraw.setPenRadius(penRadius);
    }

    abstract public void draw();

    abstract public void changeColor();
}
