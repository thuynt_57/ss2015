package src.racing;

/**
 * Created by dse on 04/12/2015.
 */
public class DemoRace {
    public static void main(String[] args) {
        Data data = new Data();

        Thread a = new Thread(new DataRace(data));
        Thread b = new Thread(new DataRace(data));
        Thread c = new Thread(new DataRace(data));

        a.start();
        b.start();
        c.start();

        try {
            a.join();
            b.join();
            c.join();
        } catch (InterruptedException ignored) { }

        System.out.print("Should be 0: "+ data.x);
    }
}
