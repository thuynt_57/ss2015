package src.racing;

/**
 * Created by dse on 04/12/2015.
 */
public class DataRace implements Runnable {

    private final Data data;

    DataRace(Data data) {
        this.data = data;
    }

    public void run() {
        for (int i = 0; i < 10000; i++) {
            data.x++;
            data.x--;
        }
    }
}