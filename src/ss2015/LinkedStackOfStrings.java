package ss2015;

import java.util.Iterator;

/******************************************************************************
 *  Compilation:  javac LinkedStackOfStrings.java
 *  Execution:    java LinkedStackOfStrings
 *  Dependencies: StdIn.java StdOut.java
 *
 *  A stack of strings, implemented using a linked list.
 *  
 *  % more tobe.txt 
 *  to be or not to - be - - that - - - is 
 * 
 *  % java LinkedStackOfStrings < tobe.txt 
 *  to be not that or be
 *  
 ******************************************************************************/

import java.util.NoSuchElementException;
import java.util.Scanner;

import libForRandomWalk.StdOut;

public class LinkedStackOfStrings<T> implements Iterable<T>{
    private int N;          // size of the stack
    private Node<T> first;     // top of stack
	private static Scanner input;

    // helper Node class
    private static class Node<T> {
        private T item;
        private Node<T> next;
    }
    
	@Override
	public MyIterator<T> iterator() {
		// TODO Auto-generated method stub
		return new MyIterator<T>();
	}
	
	@SuppressWarnings("hiding")
	private class MyIterator<T> implements Iterator<T>{
		Node<T> cur = (Node<T>) first;

		@Override
		public boolean hasNext() {
			// TODO Auto-generated method stub
			return cur != null;
		}

		@SuppressWarnings("unchecked")
		@Override
		public T next() {
			T result = cur.item;
			cur = cur.next;
			return result;
		}
		
	}

    public LinkedStackOfStrings() {
        N = 0;
        first = null;
    }

    // is the stack empty?
    public boolean isEmpty() { return first == null; }

    // number of elements on the stack
    public int size() { return N; }


    // add an element to the stack
    public void push(T item) {
        Node<T> oldfirst = (Node<T>) first;
        first = (Node<T>) new Node<T>();
        first.item = (T) item;
        first.next = (Node<T>) oldfirst;
        N++;
    }

    // delete and return the most recently added element
    public T  pop() {
        if (isEmpty()) throw new NoSuchElementException("Stack underflow");
        T item = (T) first.item;      // save item to return
        first = first.next;            // delete first node
        N--;
        return item;                   // return the saved item
    }

    @SuppressWarnings("hiding")
	public static <T extends Comparable<T>> T[] sort(T[] a){
    	if (a == null || a.length == 0) return null;
    	for (int i = 0; i < a.length - 1; i++)
    		for (int j = i; j < a.length; j++)
    			if (a[i].compareTo(a[j]) > 0) {
    				T tem = a[i];
    				a[i] = a[j];
    				a[j] = tem;
    			}
		return a;
    }
    

    // test client
    public static void main(String[] args) {
        LinkedStackOfStrings<String> s = new LinkedStackOfStrings<String>();
//        input = new Scanner(System.in);
//        int i = 0;
//        while (input.hasNext()) { //(!StdIn.isEmpty()) {
//        	i++;
//        	System.err.println("Input " + i +" :");
//            String item = input.next();//StdIn.readString();
//        	if (item.equals("/")) break;
//            if (!item.equals("-")) s.push(item); 
//            else if (s.isEmpty())  StdOut.println("BAD INPUT"); 
//            else                   StdOut.print(s.pop());
//
//        } 
//        System.err.println("My list :");
//        for (String it: s)
//        	System.out.println (it);
    	
    	String[] words = {"abc", "ace", "acd"};
        s.sort(words);
        System.out.print("After sorting: ");
        for (String word: words)
        	System.out.print(word + " ");
    }


}

