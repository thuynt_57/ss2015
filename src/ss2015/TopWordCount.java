package ss2015;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;


public class TopWordCount {
	static Map<String, Integer> wordMap = new HashMap<String, Integer>();
	
	public static void main (String args[]){
		// read input to the hash map
		int L = Integer.parseInt(args[0]);
		Scanner input = new Scanner(System.in);
		String key;
		while (input.hasNext()) {
			key = input.next();
			if (key.length() >= L) {
	            Integer freq = wordMap.get(key);
	            wordMap.put(key, (freq == null) ? 1 : freq + 1);    
			}
		}
		input.close();
		
		// find top word
		int maxValue = 0;
		String topWord = null;
		for (String k : wordMap.keySet()) {
			if(wordMap.get(k) > maxValue) {
				maxValue = wordMap.get(k);
				topWord = k;
			}
		}
		
		// print it
		System.out.println(topWord + " " + maxValue);
	}
}
