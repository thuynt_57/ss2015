package ss2015;


//goi count[c][l] la so tu do dai l tan cung bang c thi ta co count(c, l) = tong tat ca cac 
//count[x, l-1] voi dieu kien x co the dung truoc c
//nhu vay ta xay dung dan tu count[?, 1], count[?, 2].... cho den het vung quan tam
//moi lan xay dung mot cot
import java.util.Scanner;

public class InterestingLanguage {
	private int[][] constrains;
	private int nTestCase;
	private int wordLengths[];
	private char chars[];
	private int[][] resultTable;
	private static final int NUM_OF_CHAR = 26;
	private static final char[] ENGLISH_ALPHABETA = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
	
	public void readInput(){
		Scanner keyboard = new Scanner(System.in);
		constrains = new int[NUM_OF_CHAR][NUM_OF_CHAR];
		for (int i = 0; i < NUM_OF_CHAR; i++) {
			constrains[i] = new int[NUM_OF_CHAR];
			for (int j = 0; j < NUM_OF_CHAR; j++) {
				constrains[i][j] = keyboard.nextInt();
			}
		}
		nTestCase = keyboard.nextInt();
		wordLengths = new int[nTestCase];
		chars = new char[nTestCase]; 
		for (int i = 0; i < nTestCase; i++) {
			chars[i] = keyboard.next().charAt(0);
			wordLengths[i] = keyboard.nextInt();	
		}
	}
	
	private int findIndexOf(char c) {
		for (int i = 0; i < NUM_OF_CHAR; i++) {
			if (c == ENGLISH_ALPHABETA[i])
				return i;
		}
		return -1;
	}
	
	// true if char1 can stand before char2
	private boolean canStandBefore (int _indexOfChar1, int _indexOfChar2) {
		if (constrains[_indexOfChar1][_indexOfChar2] == 1) 
			return true;
		else return false;
	}
	
	private void buidResultTable(int _maxLengthOfWord){
		resultTable = new int[NUM_OF_CHAR][_maxLengthOfWord + 1];
		for (int _indexOfColumn = 1; _indexOfColumn <= _maxLengthOfWord; _indexOfColumn++){
			if (_indexOfColumn == 1) {
				for (int indexOfChar = 0; indexOfChar < NUM_OF_CHAR; indexOfChar++) {
					resultTable[indexOfChar][1] = 1;
				}
			}else {
				for (int indexOfChar = 0; indexOfChar < NUM_OF_CHAR; indexOfChar++) {
					resultTable[indexOfChar][_indexOfColumn] = 0;
					for (int i = 0; i < NUM_OF_CHAR; i++) { 
						if (canStandBefore(i,indexOfChar)) {
							resultTable[indexOfChar][_indexOfColumn] += resultTable[i][_indexOfColumn - 1];
							resultTable[indexOfChar][_indexOfColumn] = resultTable[indexOfChar][_indexOfColumn] % 1000000007;
						}
					}
				}
			}
		}
	}
	
	public void printOutput() {
		for (int i = 0; i < nTestCase; i++) {
			System.out.println(resultTable[findIndexOf(chars[i])][wordLengths[i]]); 
		}
	}
	
	public static void main (String agrs[]) {
		InterestingLanguage iLang = new InterestingLanguage();
		iLang.readInput();
		iLang.buidResultTable(100000);
		iLang.printOutput();
	}
}
