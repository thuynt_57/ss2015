package ss2015;

import java.io.IOException;
import java.util.Scanner;


public class RoyLifeCycle {
	private static
		int nDay;
		static String activities[];
		
		static int longestCodingStreakOfADay (String activitiesInDay) {
			int longestOfADay = 0;
    		int charIndex = 0;
    		int length = activitiesInDay.length();
    		
	    	while (charIndex < length-2) {		     		
		    	int count = 0;
	    		while (activitiesInDay.charAt(charIndex) == 'C') {	
	    			charIndex++;
	    			count++;
	    			if (charIndex >= length) break;
	    		}
	    		if (count > longestOfADay) {
	    			longestOfADay = count;
	    		}
	    		charIndex++;
	    	}
			return longestOfADay;
		}
	    
	    static int longestCodingStreakOfTheDay () {
	    	int longestOfTheDay = 0;
	    	int longestOfDay[] = new int[nDay];
	    	for (int i = 0; i < nDay; i++) {
	    		longestOfDay[i] = 0;
	    	}

	    	for (int dayIndex = 0; dayIndex < nDay; dayIndex++) {
	    		longestOfDay[dayIndex] = longestCodingStreakOfADay(activities[dayIndex]);
		    	if (longestOfDay[dayIndex] > longestOfTheDay)
		    		longestOfTheDay = longestOfDay[dayIndex];
	    	}
			return longestOfTheDay;
	    }
	    
	    static int longestCodingStreakOfNDays () {
	    	String actsAllDay = "";
	    	for (int i = 0; i < nDay; i++) {
	    		actsAllDay += activities[i];
	    	}
	    	
	    	return longestCodingStreakOfADay(actsAllDay);
	    }
	    
    public static void readInput () {
			Scanner keyboard = new Scanner(System.in);
			nDay = keyboard.nextInt();
			
			activities = new String[nDay];
			for (int i = 0; i < nDay; i++) {
				activities[i] = keyboard.next();	
			}
	    }
	public static void printOutput () {
	    	System.out.println(longestCodingStreakOfTheDay() + " " + longestCodingStreakOfNDays());
	    }
    public static void main (String args[]) throws IOException {
    	readInput();
    	printOutput();
    }
}
