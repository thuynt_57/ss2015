package ss2015;

import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Censorship {	
	public static void main(String args[]) throws IOException {
		FileReader instream = new FileReader("input_censorship.txt");
//		Scanner instream = new Scanner()
		StringBuilder sb = new StringBuilder();
		
		char[] cbuf = new char[10000]; 
		int offset = 10, length = 200;
		String KEY = "facebook"; 
		while (true) {
			int size = instream.read(cbuf, offset, length + 10);
			if(size == -1) break;
			String sbuf = cbuf.toString().substring(0,size);
			String m_sbuf = sbuf.replaceAll("(?i)" + KEY, "***");
			sb.append(m_sbuf.substring(offset, length));
		}
		
		final FileWriter outstream = new FileWriter("output_censorship.txt");
		BufferedWriter out = new BufferedWriter(outstream);
		out.write(sb.toString());;
		System.out.println("The result is wrote in file out_censorship.txt");
		
	}
}
