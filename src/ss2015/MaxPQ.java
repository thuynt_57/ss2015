package ss2015;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

// Cai dat cau truc hang doi uu tien voi heap bang cay nhi phan trong mang
public class MaxPQ<Key extends Comparable<Key>> {
	private Key[] pq;
	private int N; 
	@SuppressWarnings("unchecked")
	public MaxPQ(int capacity){
		pq = (Key[]) new Comparable[capacity+1];
	}
	public MaxPQ(Key[] a){
		pq = a;
	}
	
	private boolean less(int pa, int ch){
		if ( pq[pa].compareTo(pq[ch]) < 0) return true;
		return false;
	}
	
	void exch(int pa, int ch) {
		Key tem = pq[pa];
		pq[pa] = pq[ch];
		pq[ch] = tem;
	}
	
	private void swim(int k) {
		while (k > 1 && less(k/2, k)){
			exch(k, k/2);
			k = k/2;
		}
	}
	public void insert(Key v){
		pq[++N] = v;
		swim(N);
	}
	
	private void sink(int k) {
		while (2*k <= N) {
			int j = 2*k;
			if (j < N && less(j, j+1)) j++;
			if (!less(k, j)) break;
			exch(k, j);
			k = j;
		}
	}

	public Key delMax(){
		Key max = pq[1];
		exch(1, N--);
		sink(1);
		pq[N+1] = null;
		return max;
	}
	public boolean isEmpty(){
		return N == 0;
	}
	public Key max(){
		return pq[N-1];
	}
	public int size(){
		return N;
	}
	
	public Key getItem(int index) {
		return pq[index];
	}

	public static void main(String args[]) throws IOException {
		int M = Integer.parseInt(args[0]);
		MaxPQ<Integer> maxPQ = new MaxPQ<Integer>(M+1);

		Scanner inFile = new Scanner(System.in);
		while (inFile.hasNext()) {
			maxPQ.insert(inFile.nextInt());
			if (maxPQ.size() >  M)
				maxPQ.delMax();
		}
		inFile.close();
			
		FileWriter fstream = new FileWriter("output_maxPQ.txt");
		BufferedWriter out = new BufferedWriter(fstream);
		System.out.println("The result is wrote in file out_maxPQ.txt");
		for (int i = 1; i <= maxPQ.size(); i++) {
			out.write(maxPQ.getItem(i) + "\n");
		}
		out.close();
	}
}
