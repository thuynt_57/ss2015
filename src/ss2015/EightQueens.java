package ss2015;

import java.util.Scanner;

public class EightQueens {
	private static boolean isSafe (int row, int[] pos){
//		System.out.println("Row: " + row);
		for (int i = 0; i < pos.length; i++) {
			if (pos[i] != -1 && i!= row)
				if ( (row + pos[row]) == (i + pos[i]) || (row - pos[row]) == (i - pos[i]) || pos[row] == pos[i] ) {
//					System.out.println(i + " " + pos[i]);
					return false;
				}
		}
		return true;
	}
	
	public static boolean arrange(int row, int[] pos) {
		if (row < 0 ) return true;
		for (int column = 0; column < pos.length; column ++) {
			pos[row] = column;
			for (int i = row - 1; i >= 0; i--) {
				pos[i] = -1;
			}
			if (isSafe(row, pos)) {
				System.out.println("Safe: " + row + " " + pos[row]);
				if (arrange(row-1, pos)) return true;
			}
		}
		return false;
	}
	
	
	public static void main(String[] args){
		System.out.print("Enter the size of table: ");
		int[] pos;
		Scanner keyboard = new Scanner(System.in);
		int sizeOfTable = keyboard.nextInt();
		pos = new int[sizeOfTable];
		for (int i = 0; i < sizeOfTable; i++)
			pos[i] = -1;
		keyboard.close();
		
		arrange(sizeOfTable - 1, pos);
		
		if (arrange(sizeOfTable - 1, pos)) {
			for (int i = 0; i < sizeOfTable; i++) {
				for (int j = 0; j < sizeOfTable; j++){
					if (j == pos[i]) System.out.print("*");
					else System.out.print(".");
				}
				System.out.println();
			}
		}
		else 
			System.out.print("No solution found.");
	}
}
