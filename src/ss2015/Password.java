package ss2015;

import java.util.Scanner;

public class Password {
	private int numPass;
	private String[] passwords;
	private boolean isPalindrome (String str1, String str2) {
		if (str1.length() != str2.length())
			return false;
		else {
			for (int i = 0; i < str1.length(); i++)
				if (str1.charAt(i) != str2.charAt(str1.length() - 1 - i))
					return false;
		}
		return true;
	}
	
	private void findPasswordAndPrintOutput () {
		for (int i = 0; i < numPass; i++) {
			for (int j = i; j < numPass; j++) {
				if (isPalindrome(passwords[i], passwords[j])) {
					System.out.print(passwords[i].length() + " " + passwords[i].charAt(passwords[i].length()/2));
					return;
				}
			}
		}
	}
	
    public void readInput () {
		Scanner keyboard = new Scanner(System.in);
		numPass = keyboard.nextInt();		
		passwords = new String[numPass];
		for (int i = 0; i < numPass; i++) {
			passwords[i] = keyboard.next();	
		}
    }
    
    public static void main (String agrs[]) {
    	Password pass = new Password();
    	pass.readInput();
    	pass.findPasswordAndPrintOutput();
    }
}
