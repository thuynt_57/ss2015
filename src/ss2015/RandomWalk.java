package ss2015;

import libForRandomWalk.StdDraw;
import libForRandomWalk.StdOut;

/******************************************************************************
 *  Compilation:  javac RandomWalk.java
 *  Execution:    java RandomWalk N
 *  Dependencies: StdDraw.java
 *
 *  % java RandomWalk 20
 *  total steps = 300
 *
 *  % java RandomWalk 50
 *  total steps = 2630
 *
 *  Simulates a 2D random walk and plots the trajectory.
 *
 *  Remarks: works best if N is a divisor of 600.
 *
 ******************************************************************************/

public class RandomWalk { 
	public static void goTenStepToTurn(int N) {
        StdDraw.setXscale(-N, +N);
        StdDraw.setYscale(-N, +N);
        StdDraw.clear(StdDraw.GRAY);
        int x = 0, y = 0;
        char direction = 'L';
        
        while (Math.abs(x) < N && Math.abs(y) < N) {
        	double r = Math.random();
	        if (r < 0.25 && direction != 'L') { 	
				for (int i = 0; i < 10; i++) {
		            StdDraw.setPenColor(StdDraw.WHITE);
		            StdDraw.filledSquare(x, y, 0.45);
		            x--;
		            StdDraw.setPenColor(StdDraw.BLUE);
		            StdDraw.filledSquare(x, y, .45);
		            StdDraw.show(40);
				}
	        	direction = 'L';
	        }
	        else if (r < 0.50 && direction != 'R') {
				for (int i = 0; i < 10; i++) {
		            StdDraw.setPenColor(StdDraw.WHITE);
		            StdDraw.filledSquare(x, y, 0.45);
		            x++;
		            StdDraw.setPenColor(StdDraw.BLUE);
		            StdDraw.filledSquare(x, y, .45);
		            StdDraw.show(40);
				}
	        	direction = 'R';
	        }
	        else if (r < 0.75 && direction != 'D') {
				for (int i = 0; i < 10; i++) {
		            StdDraw.setPenColor(StdDraw.WHITE);
		            StdDraw.filledSquare(x, y, 0.45);
		            y--;
		            StdDraw.setPenColor(StdDraw.BLUE);
		            StdDraw.filledSquare(x, y, .45);
		            StdDraw.show(40);
				}
	        	direction = 'D';
	        }
	        else if (r < 1.00 && direction != 'U'){
				for (int i = 0; i < 10; i++) {
		            StdDraw.setPenColor(StdDraw.WHITE);
		            StdDraw.filledSquare(x, y, 0.45);
		            y++;
		            StdDraw.setPenColor(StdDraw.BLUE);
		            StdDraw.filledSquare(x, y, .45);
		            StdDraw.show(40);
				}
	        	direction = 'U';
	        }
		}
	}
	
	public static void goSpiral(int N) {
		StdDraw.setXscale(-N, +N);
        StdDraw.setYscale(-N, +N);
        StdDraw.clear(StdDraw.GRAY);
        int x = 0, y = 0;
        int radius = 1;
        
        while (Math.abs(x) < N && Math.abs(y) < N) {
        	for (int i = 0; i < radius; i++) {
	            StdDraw.setPenColor(StdDraw.WHITE);
	            StdDraw.filledSquare(x, y, 0.45);
	            x++;
	            StdDraw.setPenColor(StdDraw.BLUE);
	            StdDraw.filledSquare(x, y, .45);
	            StdDraw.text(x, y, "3");
	            StdDraw.show(40);
        	}
        	
        	for (int i = 0; i < radius; i++) {
	            StdDraw.setPenColor(StdDraw.WHITE);
	            StdDraw.filledSquare(x, y, 0.45);
	            y++;
	            StdDraw.setPenColor(StdDraw.BLUE);
	            StdDraw.filledSquare(x, y, .45);
	            StdDraw.text(x, y, "3");
	            StdDraw.show(40);
        	}
        	radius++;
        	for (int i = 0; i < radius; i++) {
	            StdDraw.setPenColor(StdDraw.WHITE);
	            StdDraw.filledSquare(x, y, 0.45);
	            x--;
	            StdDraw.setPenColor(StdDraw.BLUE);
	            StdDraw.filledSquare(x, y, .45);
	            StdDraw.text(x, y, "3");
	            StdDraw.show(40);
        	}
        	 
        	for (int i = 0; i < radius; i++) {
	            StdDraw.setPenColor(StdDraw.WHITE);
	            StdDraw.filledSquare(x, y, 0.45);
	            y--;
	            StdDraw.setPenColor(StdDraw.BLUE);
	            StdDraw.filledSquare(x, y, .45);
	            StdDraw.text(0.2, 0.5, "3");
	            StdDraw.show(40);
        	}
        	radius++;
        	
		}
	}
	
    public static void main(String[] args) {
        int N = Integer.parseInt(args[0]);
//        goTenStepToTurn(N);
        goSpiral(N);
//        StdDraw.setXscale(-N, +N);
//        StdDraw.setYscale(-N, +N);
//        StdDraw.clear(StdDraw.GRAY);
//
//        int x = 0, y = 0;
//        int steps = 0;
//        while (Math.abs(x) < N && Math.abs(y) < N) {
//            StdDraw.setPenColor(StdDraw.WHITE);
//            StdDraw.filledSquare(x, y, 0.45);
//            double r = Math.random();
//            if      (r < 0.25) x--;
//            else if (r < 0.50) x++;
//            else if (r < 0.75) y--;
//            else if (r < 1.00) y++;
//            steps++;
//            StdDraw.setPenColor(StdDraw.BLUE);
//            StdDraw.filledSquare(x, y, .45);
//            StdDraw.show(40);
//        }
//        StdOut.println("Total steps = " + steps);
    }

}