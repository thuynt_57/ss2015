package ss2015;

import java.util.Random;
class Chopstick extends Thread {
	private int id;
	private boolean free;
	public Chopstick(int _id) {
		id = _id;
		free = true;
	}
	public void setState(boolean _free){
		free = _free;
	}
	public boolean isFree(){
		return free == true;
	}
}

class Philosopher extends Thread {
	private String name;
	private Chopstick left, right;
	private int MAX_TIME = 1000;
	private int eat_times = 0;
	private long eat_time = 0, think_time = 0;
	public static long start_time = 0;
	// each Philosopher is assigned an integer id and two chopsticks
	public Philosopher(int id, Chopstick c1, Chopstick c2) {
		name = "Philosopher-" + id;
		left = c1;
		right = c2;
	}
	
	private void thinks() throws InterruptedException{
		System.out.println(name + " thinking");
		// TODO: think for random number of milliseconds <=1 sec
		// TODO: acquire both chopsticks
		Random rand = new Random();
		int randTimeToThink = rand.nextInt(MAX_TIME + 1);
		sleep(randTimeToThink);
		think_time += randTimeToThink;
	}
	private void eats() throws InterruptedException{
		left.setState(false);
		right.setState(false);
		System.out.println(name + "dining");
		// TODO: eat for random number of milliseconds <= 1sec
		// TODO: release both chopsticks
		Random rand = new Random();
		int randTimeToEat = rand.nextInt(MAX_TIME + 1);
		sleep(randTimeToEat);
		eat_times++;
		eat_time += randTimeToEat;
		left.setState(true);
		right.setState(true);
	}
	
	// the run method is invoked by calling the start()method on
	// an instance of the Philosopher class, e.g., phil.start()
	public void run() {	
		while (System.currentTimeMillis() - start_time <= 60*1000) { // loop 1 minute
			try {
				thinks();
				if (left.isFree() && right.isFree()) {
					eats();
				}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		printResult();
	}
	
	public void printResult(){
		System.out.println(name);
		System.out.println("Number of eat time: " + eat_times);
		System.out.println("Eat time: " + eat_time + "  Think time:" + think_time);
	}
}

public class DinningPhil {
	public static void main(String[] args) {
		Philosopher.start_time = System.currentTimeMillis();
		Chopstick[] chopArr = new Chopstick[5];
		Philosopher[] philArr = new Philosopher[5];
		for (int i = 0; i < chopArr.length; i++) {
			chopArr[i] = new Chopstick(i);
		}
		for (int i = 0; i < philArr.length; i++) {
			if (i == philArr.length - 1){
				philArr[i] = new Philosopher(i, chopArr[i], chopArr[0]);
				break;
			}
			philArr[i] = new Philosopher(i, chopArr[i], chopArr[i+1]);
		}
		
		for (int i = 0; i < philArr.length; i++) {
			philArr[i].start();
		}		
	}
}

