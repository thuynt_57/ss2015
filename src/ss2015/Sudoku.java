package ss2015;

import java.util.Scanner;


public class Sudoku {
	static int N = 9;
	
	private static boolean isLegal(int _row, int _column, int[][] _nums) {
		for (int i = 0; i < N; i++) 
			if (_nums[_row][_column] == _nums[i][_column] && _row != i)
				return false;
		
		for (int indexOfColumn = 0; indexOfColumn < N; indexOfColumn++) 
			if (_nums[_row][_column] == _nums[_row][indexOfColumn] && _column != indexOfColumn)
				return false;
		
		int x = _row - _row % 3, y = _column - _column % 3;
		for (int i = x; i < x + 3; i++)
			for (int j = y; j < y + 3; j++)
				if (_nums[_row][_column] == _nums[i][j] && _row != i && _column != j)
					return false;
		return true;
	}
	
	
	
	public static boolean fillNumber(int _row, int _column, int[][] _nums) {
		if (_row < 0 ) return true;
		if (_nums[_row][_column] == 0) {
			for (int num = 1; num <= N; num++) {
				_nums[_row][_column] = num;
	//			System.out.println("try (" + _row + " ," + _column + " ) = " + num);
				if (isLegal(_row, _column, _nums)) {
//					System.out.println("isLegal (" + _row + " ," + _column + " ) =" + num);
					int nextRow, nextColumn;
					if (_column == 0){
						nextRow = _row - 1 ;
						nextColumn = N - 1;
					}
					else {
						nextRow = _row;
						nextColumn = _column - 1;
					}
						
					if (fillNumber(nextRow, nextColumn, _nums)) return true;			
				}
				_nums[_row][_column] = 0;
			}
			
		} else {
			int nextRow, nextColumn;
			if (_column == 0){
				nextRow = _row - 1 ;
				nextColumn = N - 1;
			}
			else {
				nextRow = _row;
				nextColumn = _column - 1;
			}
				
			return (fillNumber(nextRow, nextColumn, _nums));
		}
		return false;
	}
	
	public static void main(String args[]) {
		int nums[][] = new int[10][10];
		Scanner keyboard = new Scanner(System.in);
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				nums[i][j] = keyboard.nextInt();
//				System.out.print(nums[i][j]);
			}
//			System.out.println();
		}
		
		if (fillNumber(N - 1, N-1 , nums)) {
//			fillNumber(N - 1, nums);
			for (int i = 0; i < N; i++) {
				for (int j = 0; j < N; j++) {
					System.out.print(nums[i][j]);
				}
				System.out.println();
			}
		}
		else
			System.out.println("Solution not found.");
	}
}
