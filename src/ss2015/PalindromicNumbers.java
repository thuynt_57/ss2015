package ss2015;

import java.util.Scanner;

public class PalindromicNumbers {
	private int numTestCase;
	private int nums[];
	private boolean isPalindromicNum (int num) {
		String temp = Integer.toString(num);
		for (int i = 0; i < temp.length()/2; i++){
			if (temp.charAt(i) != temp.charAt(temp.length() - 1 - i))
				return false;
		}	
		return true;
	}
	private int countPalindromicNums (int a, int b) {
		int numPals = 0;
		for (int i = a; i <= b; i++)
			if (isPalindromicNum(i))
				numPals++;
		return numPals;
	}
	public void readInput () {
		Scanner keyboard = new Scanner(System.in);
		this.numTestCase = keyboard.nextInt(); 
		nums = new int[numTestCase*2];
		
		for (int i = 0; i < numTestCase*2; i+=2) {
			keyboard.nextLine();
			nums[i] = keyboard.nextInt();
			nums[i+1] = keyboard.nextInt();
		}
	}
	
	public void printOutput () {
		for (int i = 0; i < numTestCase*2; i+=2) {
			System.out.println(countPalindromicNums(nums[i], nums[i+1]));
		}
	}
	
	public static void main (String args[]) {
		PalindromicNumbers palNums = new PalindromicNumbers();
		palNums.readInput();
		palNums.printOutput();
	}
}


