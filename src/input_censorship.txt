When Mark Zuckerberg was 19 and a student at Harvard University, he wanted to find a way for his fellow Harvard colleagues to connect with each other. So in February 2004, Zuckerberg introduced Facebook (www.facebook.com) and a new era of networking began.
Today, the social networking site has more than 60 million active members, roughly the same population as the U.K. These users can now upload photos, have group discussions, and even play games on their individual profiles; they can also add one another as �friends� and connect with users who share sim�ilar interests, regardless of where they are in the world. Nowadays, more businesses and corporate folks are joining Facebook too, adding their pages to the Facebook network. Advertisers are even turning their attention to this growing market for good reason�there is strength in numbers. So what should you know about Facebook? Here are 10 things for starters.

1. Who Is Using Facebook?

Since its inception in February 2004, Facebook has grown significantly, and it now has more than 60 million active users. In comparison, MySpace has a total of 300 million users, although not all are active (�active� users are those who have logged in within the last 30 days). According to Facebook�s statistics page, the number of active users has doubled every 6 months, with 250,000 new users joining each day since January 2007 for an average of 3% growth per week. According to internet-ranking company comScore, Facebook is the sixth-most trafficked site in the U.S., with the average user spending 20 minutes a day actively using Facebook by uploading photos, sending messages, or even having discussions within a group. The highly coveted demographic (from 18 to 25 years old) is 52% of Facebook�s userbase, averaging 30 to 45 minutes each day on the site.

2. What Can You Find on Facebook?

Simply put, if people have an interest, it is part of Facebook. A user just has to enter a topic, such as �video games� or �new technology,� into the search box and then hit the �search� button. Up to 1,000 profiles are displayed, 20 at a time, starting with people in the user�s network. If a user who belongs to the University of North Carolina (UNC) network is searching for basketball fans, the results returned would be people in the UNC network first, followed by those in other networks.

From that point, a user can contact others by clicking �send message� or, if that user has a group, by clicking on the �invite to group� button. The user will see an increased number of members joining and participating in the group�s message board discussions.

Facebook also has a �poke� feature, which, in most circles, is regarded as a form of online flirtation, comparable to match.com�s �winks.� When one user is poked by another, a notification appears on the user�s homepage, allowing him or her to either �poke back� the other user or �hide poke,� which makes the poke disappear.

3. Why Are People Using Facebook?