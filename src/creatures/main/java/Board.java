import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * Created by dse on 12/14/2014.
 */
public class Board extends JPanel {

    public static final int CELL_SIZE = 10;
    private final int rows = 10;
    private final int cols = 20;
    ArrayList<Creature> creatureList;

    Board() {
        super();
        setBackground(Color.white);
        setPreferredSize(new Dimension(cols * CELL_SIZE, rows * CELL_SIZE));

        creatureList = new ArrayList<Creature>();
        creatureList.add(new Bacteria(this, 1, 1));
    }

    @Override
    public void paintComponent(Graphics g) {
        System.err.println("Board.repaint");
        super.paintComponent(g);
        for (Creature creature: creatureList) {
            creature.draw(g);
        }
    }

//    public void tick() {
//        dead = new ArrayList<Creature>();
//        babies = new ArrayList<Creature>();
//
//        for (Creature creature: creatureList) {
//            creature.tick();
//        }
//        creatureList.addAll(babies);
//        creatureList.removeAll(dead);
//    }

    public void addBaby(Creature baby) {
        if (baby != null) creatureList.add(baby);
    }

    public void addDead(Creature deadCreature) {
        if (deadCreature != null) creatureList.remove(deadCreature);
    }

    public boolean emptyCell(int x, int y) {
        if (x < 0 || y < 0 || x >= cols || y >= rows) return false;

        for (Creature creature : creatureList) {
            if (creature.atPosition(x, y)) {
                return false;
            }
        }
        return true;
    }

    public String getPopulation() {
        return String.format("%d", creatureList.size());
    }

    public void start() {
        for (Creature creature : creatureList) {
            creature.start();
        }
    }

    public boolean isEmpty() {
        return creatureList.isEmpty();
    }
}