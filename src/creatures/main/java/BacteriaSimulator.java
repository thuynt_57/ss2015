import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by dse on 12/14/2014.
 */
public class BacteriaSimulator extends JFrame {
    private Board board;
    private JLabel label;
    private Timer timer = new Timer(200, new TimerActionListener());

    public BacteriaSimulator() {
        initUI();
        setTitle("Bacteria Simulator");
        setSize(350, 200);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    private void initUI() {
        JPanel panel = new JPanel();

        board = new Board();
        panel.add(board);

        label = new JLabel(board.getPopulation());
        panel.add(label);

        JButton startButton = new JButton("Start");
        startButton.addActionListener(new StartButtonListener());
        panel.add(startButton);

        add(panel);
        pack();
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                BacteriaSimulator ex = new BacteriaSimulator();
                ex.setVisible(true);
            }
        });
    }

    private class StartButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            board.start();
            timer.start();
            label.setText(board.getPopulation());
            repaint();
        }
    }

    private class TimerActionListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            label.setText(board.getPopulation());
            repaint();

            if (board.isEmpty()) {
                timer.stop();
            }
        }
    }
}