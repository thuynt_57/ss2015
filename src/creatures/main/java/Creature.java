import java.awt.*;

/**
 * Created by dse on 12/14/2014.
 */
abstract public class Creature implements Runnable{
    protected final Board board;

    protected int age; // age of the creature measures in ticks
    protected int x; // (x,y) is the position of the creature
    protected int y;
    protected boolean dead = false;
    private Thread myThread;

    Creature(Board board, int _x, int _y) {
        this.board = board;
        x = _x;
        y = _y;
    }

    //Draw a circle representing the creature at its position
    //This function is done.
    public void draw(Graphics g) {
        System.err.println("Creature draw: " + age);
        g.setColor(getColor());
        g.fillOval(x * board.CELL_SIZE, y * board.CELL_SIZE, board.CELL_SIZE, board.CELL_SIZE);
    }

    abstract protected Color getColor();

    public void tick() {
        age++;
        System.err.println("Creature: " + age);
        if (age > getMaxAge()) {
            die();
        } else {
            if (canHaveBaby()) {
                reproduce();
            }
        }
    }

    protected void die() {
        dead = true;
        board.addDead(this);
    }

    protected abstract int getMaxAge();

    public void reproduce() {
    }

    protected boolean canHaveBaby() {
        return false;
    }

    final public boolean atPosition(int _x, int _y) {
        return (x==_x && y==_y);
    }

    public void start() {
        myThread = new Thread(this);
        myThread.start();
    }

    public void run() {
        do {
            tick();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                break;
            }
        } while (alive());
    }

    private boolean alive() {
        return !dead;
    }
}
